 MODULE UPDATE_BCS
    !-----------------------------------------------------------------------
    ! 
    ! Purpose:
    !   Update boundary condition atlases to reflect current conditions
    !
    ! PUBLIC:  Update_Radiation_BC(time)
    !          Update_Dirichlet_BC(time)
    !
    !    Jim Sicilian, CCS-2 (sicilian@lanl.gov)
    !    May 2003 and November 2004
    !
    !----------------------------------------------------------------------
 
 
    Private
    Public Update_Radiation_BC, Update_Dirichlet_BC, Update_HTC_External_BC
    Public Update_Wave_BC

    CONTAINS

    SUBROUTINE Update_Radiation_BC(time)
    !-----------------------------------------------------------------------
    !
    ! Purpose:
    !   Update Radiation Boundary Environment Temperatures
    !
    !    Jim Sicilian, CCS-2 (sicilian@lanl.gov)
    !
    !-----------------------------------------------------------------------
    use kind_module,            only: real_kind, int_kind
    use bc_operations,          only: BC_Operator, BC_Atlas, BC_Spec_Get_Operator, &
                                      BC_Op_Get_Atlas, BC_Get_Face, BC_Get_Cell,   &
                                      BC_Get_Offset, BC_Get_Length, BC_Get_Values, &
                                      BC_Get_Positions, DATA_SIZE, BC_RADIATION_Op,&
                                      TEMPERATURE_BC, BC_Get_DOF
    use error_module,           only: PUNT
    use tabular_utilities,      only: tabular_linear_interp

    implicit none

    ! arguments
    real(kind=real_kind), INTENT(IN)           :: time
    ! local variables
    real(real_kind)                            :: T0, tlast
    integer(int_kind)                          :: k,n, status
    integer(int_kind)                          :: DegreesOfFreedom, InterpolationPoints

    ! BC-related local variables.
    integer(int_kind)                          :: NumberBdyPts, BdyPt, BdyCell
    integer(int_kind)                          :: BdyFace, BdyOffSet
    type(BC_Operator), pointer                 :: RADIATION_Operator
    type(BC_Atlas),    pointer                 :: RADIATION_Atlas
    integer(int_kind), pointer, dimension(:)   :: BdyFaceList
    integer(int_kind), pointer, dimension(:)   :: BdyCellList
    integer(int_kind), pointer, dimension(:)   :: BdyOffsetList
    integer(int_kind), pointer, dimension(:)   :: BdyLengthList
    real(real_kind),   pointer, dimension(:,:) :: BdyValueList
    real(real_kind),   pointer, dimension(:,:) :: BdyPositionList

    real(kind=real_kind), allocatable, SAVE, dimension(:)  :: timelist, templist

    ! Apply the RADIATION Operator
    ! Get the RADIATION operator.
    RADIATION_Operator => BC_Spec_Get_Operator(TEMPERATURE_BC, BC_RADIATION_Op)
    
    ! Since we are doing overwrite, we can do all boundary faces in one loop.
    ! So we want to traverse the whole atlas.
    RADIATION_Atlas => BC_OP_Get_Atlas(RADIATION_Operator)
    
    ! We are using whole arrays, so faster to point at them than get a private copy.
    BdyFaceList     => BC_Get_Face(RADIATION_Atlas)
    BdyCellList     => BC_Get_Cell(RADIATION_Atlas)
    BdyOffsetList   => BC_Get_Offset(RADIATION_Atlas)
    BdyLengthList   => BC_Get_Length(RADIATION_Atlas)
    BdyValueList    => BC_Get_Values(RADIATION_Atlas)
    BdyPositionList => BC_Get_Positions(RADIATION_Atlas)
    DegreesOfFreedom = BC_Get_DOF(RADIATION_Atlas)
    
    InterpolationPoints = DegreesOfFreedom - 2

    if(.not.allocated(timelist)) then
       allocate(timelist(InterpolationPoints), STAT = status)
       if(status /=0) call punt ((/'timelist(InterpolationPoints) allocation failed'/), 'Update_Radiation_BC')
       allocate(templist(InterpolationPoints), STAT = status)
       if(status /=0) call punt ((/'templist(InterpolationPoints) allocation failed'/), 'Update_Radiation_BC')
    endif

    NumberBdyPts = DATA_SIZE(RADIATION_Atlas)
    RAD_BDY_LOOP: do BdyPt = 1, NumberBdyPts
       BdyCell = BdyCellList(BdyPt)
       BdyFace = BdyFaceList(BdyPt)
       BdyOffSet = BdyOffsetList(BdyPt)
       ! create lists of time and temperature values
       k=3
       n=1
       tlast = -1.0
       COUNT_VALUES:  do while (k<InterpolationPoints)
           if(BdyValueList(k,BdyOffset) <= tlast) then
               exit COUNT_VALUES
           endif
           timelist(n) = BdyValueList(k, BdyOffset)
           tlast = timelist(n)
           templist(n) = BdyValueList(k+1, BdyOffset)
           n=n+1
           k=k+2
       end do COUNT_VALUES
       if(n-1>1) then
           BdyValueList(2, BdyOffset) = tabular_linear_interp(time, timelist(1:n-1), templist(1:n-1))
       else
!  needed to update model for sensitivity analysis
           BdyValueList(2, BdyOffset) = BdyValueList(4, BdyOffset) 
       endif
    end do RAD_BDY_LOOP

!    call Output_Atlas_To_XML(RADIATION_Atlas)
    return
    end subroutine Update_Radiation_Bc

    SUBROUTINE Update_Dirichlet_BC(time)
    !-----------------------------------------------------------------------
    !
    ! Purpose:
    !   Update Dirichlet Boundary Temperatures
    !
    !    Jim Sicilian, CCS-2 (sicilian@lanl.gov)
    !
    !-----------------------------------------------------------------------
    use kind_module,            only: real_kind, int_kind
    use bc_operations,          only: BC_Operator, BC_Atlas, BC_Spec_Get_Operator, &
                                      BC_Op_Get_Atlas, BC_Get_Face, BC_Get_Cell,   &
                                      BC_Get_Offset, BC_Get_Length, BC_Get_Values, &
                                      BC_Get_Positions, DATA_SIZE, BC_DIRICHLET_Op,&
                                      TEMPERATURE_BC, BC_Get_DOF
    use error_module,           only: PUNT
    use tabular_utilities,      only: tabular_linear_interp

    implicit none

    ! arguments
    real(kind=real_kind), INTENT(IN)           :: time
    ! local variables
    real(real_kind)                            :: T0, tlast
    integer(int_kind)                          :: k,n, status
    integer(int_kind)                          :: DegreesOfFreedom, InterpolationPoints

    ! BC-related local variables.
    integer(int_kind)                          :: NumberBdyPts, BdyPt, BdyCell
    integer(int_kind)                          :: BdyFace, BdyOffSet
    type(BC_Operator), pointer                 :: DIRICHLET_Operator
    type(BC_Atlas),    pointer                 :: DIRICHLET_Atlas
    integer(int_kind), pointer, dimension(:)   :: BdyFaceList
    integer(int_kind), pointer, dimension(:)   :: BdyCellList
    integer(int_kind), pointer, dimension(:)   :: BdyOffsetList
    integer(int_kind), pointer, dimension(:)   :: BdyLengthList
    real(real_kind),   pointer, dimension(:,:) :: BdyValueList
    real(real_kind),   pointer, dimension(:,:) :: BdyPositionList

    real(kind=real_kind), allocatable, SAVE, dimension(:)  :: timelist, templist

    ! Apply the DIRICHLET Operator
    ! Get the Dirichlet operator.
    DIRICHLET_Operator => BC_Spec_Get_Operator(TEMPERATURE_BC, BC_DIRICHLET_Op)
    
    ! Since we are doing overwrite, we can do all boundary faces in one loop.
    ! So we want to traverse the whole atlas.
    DIRICHLET_Atlas => BC_OP_Get_Atlas(DIRICHLET_Operator)
    
    ! We are using whole arrays, so faster to point at them than get a private copy.
    BdyFaceList     => BC_Get_Face(DIRICHLET_Atlas)
    BdyCellList     => BC_Get_Cell(DIRICHLET_Atlas)
    BdyOffsetList   => BC_Get_Offset(DIRICHLET_Atlas)
    BdyLengthList   => BC_Get_Length(DIRICHLET_Atlas)
    BdyValueList    => BC_Get_Values(DIRICHLET_Atlas)
    BdyPositionList => BC_Get_Positions(DIRICHLET_Atlas)
    DegreesOfFreedom = BC_Get_DOF(DIRICHLET_Atlas)
    
    InterpolationPoints = DegreesOfFreedom - 1

    if(.not.allocated(timelist)) then
       allocate(timelist(InterpolationPoints), STAT = status)
       if(status /=0) call punt ((/'timelist(InterpolationPoints) allocation failed'/), 'Update_Dirichlet_BC')
       allocate(templist(InterpolationPoints), STAT = status)
       if(status /=0) call punt ((/'templist(InterpolationPoints) allocation failed'/), 'Update_Dirichlet_BC')
    endif

    NumberBdyPts = DATA_SIZE(DIRICHLET_Atlas)
    RAD_BDY_LOOP: do BdyPt = 1, NumberBdyPts
       BdyCell = BdyCellList(BdyPt)
       BdyFace = BdyFaceList(BdyPt)
       BdyOffSet = BdyOffsetList(BdyPt)
       ! create lists of time and temperature values
       k=2
       n=1
       tlast = -1.0
       COUNT_VALUES:  do while (k<InterpolationPoints)
           if(BdyValueList(k,BdyOffset) <= tlast) then
               exit COUNT_VALUES
           endif
           timelist(n) = BdyValueList(k, BdyOffset)
           tlast = timelist(n)
           templist(n) = BdyValueList(k+1, BdyOffset)
           n=n+1
           k=k+2
       end do COUNT_VALUES
       if(n-1>1) then
           BdyValueList(1, BdyOffset) = tabular_linear_interp(time, timelist(1:n-1), templist(1:n-1))
       else
!  needed to update model for sensitivity analysis
           BdyValueList(1, BdyOffset) = BdyValueList(3, BdyOffset)
       endif
    end do RAD_BDY_LOOP

!    call Output_Atlas_To_XML(DIRICHLET_Atlas)
    return
    end subroutine Update_Dirichlet_Bc

    SUBROUTINE Update_HTC_External_BC(time)
    !-----------------------------------------------------------------------
    !
    ! Purpose:
    !   Update Dirichlet Boundary Temperatures
    !
    !    Jim Sicilian, CCS-2 (sicilian@lanl.gov)
    !
    !-----------------------------------------------------------------------
    use kind_module,            only: real_kind, int_kind
    use bc_operations,          only: BC_Operator, BC_Atlas, BC_Spec_Get_Operator, &
                                      BC_Op_Get_Atlas, BC_Get_Face, BC_Get_Cell,   &
                                      BC_Get_Offset, BC_Get_Length, BC_Get_Values, &
                                      BC_Get_Positions, DATA_SIZE, BC_HTC_EXTERNAL_Op,&
                                      TEMPERATURE_BC, BC_Get_DOF
    use error_module,           only: PUNT
    use tabular_utilities,      only: tabular_linear_interp

    implicit none

    ! arguments
    real(kind=real_kind), INTENT(IN)           :: time
    ! local variables
    real(real_kind)                            :: T0, tlast
    integer(int_kind)                          :: k,n, status
    integer(int_kind)                          :: DegreesOfFreedom, InterpolationPoints

    ! BC-related local variables.
    integer(int_kind)                          :: NumberBdyPts, BdyPt, BdyCell
    integer(int_kind)                          :: BdyFace, BdyOffSet
    type(BC_Operator), pointer                 :: HTC_External_Operator
    type(BC_Atlas),    pointer                 :: HTC_External_Atlas
    integer(int_kind), pointer, dimension(:)   :: BdyFaceList
    integer(int_kind), pointer, dimension(:)   :: BdyCellList
    integer(int_kind), pointer, dimension(:)   :: BdyOffsetList
    integer(int_kind), pointer, dimension(:)   :: BdyLengthList
    real(real_kind),   pointer, dimension(:,:) :: BdyValueList
    real(real_kind),   pointer, dimension(:,:) :: BdyPositionList

    real(kind=real_kind), allocatable, SAVE, dimension(:)  :: timelist, templist

    ! Apply the HTC_External Operator
    ! Get the HTC_External operator.
    HTC_External_Operator => BC_Spec_Get_Operator(TEMPERATURE_BC, BC_HTC_EXTERNAL_Op)
    
    ! Since we are doing overwrite, we can do all boundary faces in one loop.
    ! So we want to traverse the whole atlas.
    HTC_External_Atlas => BC_OP_Get_Atlas(HTC_External_Operator)
    
    ! We are using whole arrays, so faster to point at them than get a private copy.
    BdyFaceList     => BC_Get_Face(HTC_External_Atlas)
    BdyCellList     => BC_Get_Cell(HTC_External_Atlas)
    BdyOffsetList   => BC_Get_Offset(HTC_External_Atlas)
    BdyLengthList   => BC_Get_Length(HTC_External_Atlas)
    BdyValueList    => BC_Get_Values(HTC_External_Atlas)
    BdyPositionList => BC_Get_Positions(HTC_External_Atlas)
    DegreesOfFreedom = BC_Get_DOF(HTC_External_Atlas)
    
    InterpolationPoints = DegreesOfFreedom - 2

    if(.not.allocated(timelist)) then
       allocate(timelist(InterpolationPoints), STAT = status)
       if(status /=0) call punt ((/'timelist(InterpolationPoints) allocation failed'/), 'Update_Dirichlet_BC')
       allocate(templist(InterpolationPoints), STAT = status)
       if(status /=0) call punt ((/'templist(InterpolationPoints) allocation failed'/), 'Update_Dirichlet_BC')
    endif

    NumberBdyPts = DATA_SIZE(HTC_External_Atlas)
    RAD_BDY_LOOP: do BdyPt = 1, NumberBdyPts
       BdyCell = BdyCellList(BdyPt)
       BdyFace = BdyFaceList(BdyPt)
       BdyOffSet = BdyOffsetList(BdyPt)
       ! create lists of time and temperature values
       k=3
       n=1
       tlast = -1.0
       COUNT_VALUES:  do while (k<InterpolationPoints)
           if(BdyValueList(k,BdyOffset) <= tlast) then
               exit COUNT_VALUES
           endif
           timelist(n) = BdyValueList(k, BdyOffset)
           tlast = timelist(n)
           templist(n) = BdyValueList(k+1, BdyOffset)
           n=n+1
           k=k+2
       end do COUNT_VALUES
       if(n-1>1) then
           BdyValueList(2, BdyOffset) = tabular_linear_interp(time, timelist(1:n-1), templist(1:n-1))
       else
!  needed to update model for sensitivity analysis
           BdyValueList(2, BdyOffset) = BdyValueList(4, BdyOffset)
       endif
    end do RAD_BDY_LOOP

!    call Output_Atlas_To_XML(HTC_External_Atlas)
    return
  end subroutine Update_HTC_External_BC

  Subroutine Update_Wave_BC(time)
    !---------------------------------------------------------------------------
    ! Purpose:
    !
    !   Update Incident Wave B.C.
    !
    !   Author(s): Gangfeng Ma (gma@udel.edu)
    !   Modified by Morteza Derakhti for focused and random waves
    !---------------------------------------------------------------------------
    use bc_data_module,         only: BC_Wave_Type, Amp_Wave, Depth_Incident,  &
                                      Celerity, Xstart, BC_Vel, Wave_Num,   &
                                      Tp_Wave, Cnoidal_Trough, Cnoidal_Mod, &
                                      nbc_surfaces, BC_Vof, nwave, x_breaking,y_breaking,  &
                                      t_breaking, f_i, k_i, Amp_i, Phase_i, &
                                      Tank_Slope, eta_mean, b_over_a
    use error_module,           only: PUNT
    use kind_module,            only: int_kind, real_kind, log_kind
    use gs_module,              only: EN_GATHER
    use mesh_module,            only: Cell
    use projection_data_module, only: Boundary_Flag
    use parameter_module,       only: ncells, ndim, nvc, nfc, nvf
    use bc_input_module,        only: Cnoidal, Cnoidal_Ck, Cnoidal_Cn
    use constants_module,       only: zero, one, pi
    use time_step_module,       only: dt
    use fluid_data_module,      only: Rho_Face_n
    use zone_module,            only: Zone

    implicit none

    real(real_kind), parameter  :: grav = 9.81
    integer(int_kind)           :: c, f, i, n
    real(real_kind), intent(in) :: time
    real(real_kind)             :: Eta, Atmp, Ylev, Dep, Dep_tem, Amp, Ymax, Ymin, &
                                   Segma, Wnum, Xlev, Zlev, Xc, D1, D2, D3, C2
    real(real_kind)             :: Ytrough, Cnf, Mod1, Wave_Height, Wave_Period,  &
                                   Xcn, Xsn, Xdn, Wave_Length, Phase_Speed
    real(real_kind)             :: ulinear, areturn, BC_slope, BC_beta, Utmp, Vtmp

    ! for Isolated breaking wave
    ! By: Morteza Derakhti
    real (real_kind), dimension(12000) :: Amp_waves, c_waves, k_waves, f_waves, segma_waves,  &
                                         eta_waves, utmp1, vtmp1, Ulinear1
    real (real_kind) :: xb, tb, zb, Cteta, Steta,ba
    integer (int_kind) :: nw, N_waves

    ! surface slope
    BC_slope = 0.0
    BC_beta = 0.0
    ! Default is water
    BC_Vof = one

    CELL_LOOP: do c = 1,ncells

       Ymin = 1.0E+10
       Ymax = -1.0E+10
       do f = 1,nfc
          if (Cell(c)%Face_Centroid(2,f) < Ymin) Ymin = Cell(c)%Face_Centroid(2,f)
          if (Cell(c)%Face_Centroid(2,f) > Ymax) Ymax = Cell(c)%Face_Centroid(2,f)
       end do

       FACE_LOOP: do f = 1,nfc
       
          If (Boundary_Flag(f,c) == 2) then

             ! Solitary Wave
             If (BC_Wave_Type(f,c) == 1) Then
                BC_slope = MAXVAL(Tank_Slope)
                BC_beta = atan(BC_slope)
                Dep = MAXVAL(Depth_Incident)
                Amp = MAXVAL(Amp_Wave)
                Phase_Speed = Celerity

                Atmp = dsqrt(0.75*Amp/Dep**3)
                Eta = Amp/cosh(Atmp*(Xstart-Phase_Speed*time))**2
                
                Xc = Xstart-Phase_Speed*time
                C2 = dsqrt(grav*Dep)
                D2 = 2.0*Amp*Atmp**2*(2.0*cosh(Atmp*Xc)**2-3.)  &
                              /cosh(Atmp*Xc)**4
                D1 = -2.0*Amp*sinh(Atmp*Xc)*Atmp/cosh(Atmp*Xc)**3
                D3 = -8.0*Amp*sinh(Atmp*Xc)*Atmp**3*  &
                              (cosh(Atmp*Xc)**2-3.)/cosh(Atmp*Xc)**5

                If (Ymax < (Dep+Eta)) then  ! this cell contains water only
                   Ylev = Cell(c)%Face_Centroid(2,f)
                   Utmp = C2*Eta/Dep*(1.0-1.0/4.0*Eta/Dep+Dep/3.0*(Dep/Eta)*  &
                                        (1.0-3.0/2.0*Ylev**2/Dep**2)*D2)               
                   Vtmp = -C2*Ylev/Dep*((1.0-1.0/2.0*Eta/Dep)*D1+1.0/3.0*Dep**2*  &
                                         (1.0-1.0/2.0*Ylev**2/Dep**2)*D3)
                   
                   BC_Vel(1,f,c) = Utmp*cos(BC_beta)+Vtmp*sin(BC_beta)
                   BC_Vel(2,f,c) = Vtmp*cos(BC_beta)-Utmp*sin(BC_beta)
                   BC_Vel(3,f,c) = 0.0

                   BC_Vof(f,c) = 1.0
                else if (Ymin > (Dep+Eta)) then
                   do n = 1,ndim
                      BC_Vel(n,f,c) = 0.0
                   end do

                   BC_Vof(f,c) = 0.0
                else
                   ! this face is at the interface of water and air
                   Ylev = Ymin+(Dep+Eta-Ymin)/2
                   Utmp = C2*Eta/Dep*(1.0-1.0/4.0*Eta/Dep+Dep/3.0*(Dep/Eta)*  &
                                        (1.0-3.0/2.0*Ylev**2/Dep**2)*D2)               
                   Vtmp = -C2*Ylev/Dep*((1.0-1.0/2.0*Eta/Dep)*D1+1.0/3.0*Dep**2*  &
                                         (1.0-1.0/2.0*Ylev**2/Dep**2)*D3)

                   BC_Vel(1,f,c) = Utmp*cos(BC_beta)+Vtmp*sin(BC_beta)
                   BC_Vel(2,f,c) = Vtmp*cos(BC_beta)-Utmp*sin(BC_beta)
                   BC_Vel(3,f,c) = 0.0

                   BC_Vof(f,c) = (Dep+Eta-Ymin)/(Ymax-Ymin)
                end if
             end if

             !---------------------
          end if
       end do FACE_LOOP
    end do CELL_LOOP

    Return
  End Subroutine Update_Wave_BC

  END MODULE UPDATE_BCS
